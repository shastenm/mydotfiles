### EXPORT
export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
export ALTERNATE_EDITOR=""                        # setting for emacsclient
export EDITOR="emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"           # $VISUAL use Emacs in GUI mode


### "bat" as manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

### SET VI MODE ###
# Comment this line out to enable default emacs-like bindings
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


### PROMPT
# This is commented out if using starship prompt
#PS1='[\u@\h \W]\$ '
export PS1="\[$(tput bold)\]\[$(tput setaf 4)\][\[$(tput setaf 4)\]\u\[$(tput setaf 5)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 2)\]\W\[$(tput setaf 4)\]]\[$(tput setaf 2)\]\\$ \[$(tput sgr0)\]"

### PATH
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/bin:$PATH"
export GOPATH="$HOME/go"
export PATH="$PATH:${GOPATH//://bin:}/bin"
export PATH="$PATH:${PYTHONPATH//://bin:}/bin"
export PYTHONPATH=${PYTHONPATH}:"${HOME}/Documents/dev/python"
export PYTHONPATH=${PYTHONPATH}:"${HOME}/.local/bin/python3" 

if [ -d "$HOME/bin" ] ;
  then PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "/usr/local.go/bin" ] ;
  then PATH="/usr/local.go/bin:$PATH"
fi

if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

### CHANGE TITLE OF TERMINALS
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac

### SHOPT
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize # checks term size when bash regains control

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# navigation
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# BEGIN
### ALIASES ###
# vim and emacs
alias vim="nvim"
alias em="/usr/bin/emacs -nw"
alias emacs="emacsclient -c -a 'emacs'"
alias dms="~/.emacs.d/bin/doom sync"
alias dmd="~/.emacs.d/bin/doom doctor"
alias dmu="~/.emacs.d/bin/doom upgrade"
alias dmp="~/.emacs.d/bin/doom purge"

#Redirection
alias ..="cd .. && ls -l"
alias ...="cd ../.. && ls -l"
alias ....="cd ../../.. && ls -l"
alias .....="cd ../../../.. && ls -l"
alias ......="cd ../../../../.. && ls -l"
alias .......="cd ../../../../../.. && ls -l"

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# get fastest mirrors
#alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
#alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
#alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
#alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"
#
# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

#Common Commands
alias bp="bpython"
alias d="pwd"
alias cm="sudo chmod"
alias cmm="sudo chmod 777"
alias cmx="sudo chmod +x"
alias co="sudo chown -R shasten:shasten"
alias p="python3"
alias pi="python3 -m pip install"
alias prun="python3 manage.py runserver"
alias t="sudo touch"
alias w="wget"
alias u="uname"
alias s="sudo"
alias su="sudo -s"

#alias su="sudo su"
alias mkd="sudo mkdir -pv"
alias rm="sudo rm -rf"
alias SS="sudo systemctl"
alias pff="sudo poweroff"
alias xb="xrdb -merge ~/.Xresources"

#Pacman And AUR
#alias sp="sudo pacman -S"
#alias sy="sudo pacman -Sy"
#alias syu="sudo pacman -Syu"
#alias syy="sudo pacman -Syy"
#alias syyu="sudo pacman -Syyu"
#alias ss="sudo pacman -Ss"
#alias spr="sudo pacman -R"
#alias rs="sudo pacman -Rs"
#alias rsn="sudo pacman -Rsn"
#alias rdd="sudo pacman -Rdd"
#alias sc="sudo pacman -Scn"
#alias cleanup="sudo pacman -Rns $(pacman -Qtdq)"
#alias U="sudo pacman -U"
#alias yts="yay -S"
#alias yup="yay -Syu --noconfirm"
#

#Debian and Snap Package Management
alias ai="sudo apt install"
alias ar="sudo apt remove"
alias as="sudo apt search"
alias au="sudo apt update"
alias auu="sudo apt upgrade && sudo apt upgrade"
alias asi="sudo snap install"
alias ar="sudo snap remove"
alias mi="sudo make clean install"
alias rch="sudo rm -rf config.h"
#Directories
alias hm="cd $HOME && ls -lah"
alias ds="cd ~/Desktop && ls -l"
alias dc="cd ~/Documents && ls -l"
alias dn="cd ~/Downloads && ls -l"

#Dotfile scripts
alias brc="vim ~/.bashrc" 
alias zrc="vim ~/.zshrc" 
alias xre="vim ~/.Xresources" 
alias sdk="vim ~/.config/sxhkd/sxhkdrc"
alias news="vim ~/.config/newsboat/config" 
alias spe="vim $HOME/.spectrwm.conf"
alias xmd="vim $HOME/.xmonad/xmonad.hs"

#git
alias gi="git init"
alias gb="git branch"
alias gcb="git checkout -b"
alias gh="git checkout"
alias gl="git clone"
alias gm="git commit -am"
alias gra="git remote add" 
alias grm="git remote rm"
alias gp="git push"
alias gpu="git push -u"
alias gpl="git pull"
alias gplu="git pull -u"
alias gstat="git status"

#MISC
alias yt="ytfzf -D"
alias ytt="ytfzf -t"

# ~/.config directories
alias cf="cd ~/.config && ls -l"
alias dw="cd ~/.config/dwm && ls -la"
alias xr="cd $HOME/.config/xmobar && ls -l"

# ~/.local directories
alias loc="cd ~/.local/bin"
alias scr="cd ~/.local/bin/scripts && ls -l" 
alias de="cd ~/.local/bin/scripts/dmenu && ls -l" 
alias ky="cd ~/.local/bin/scripts/dmenu/keys/ && ls -l" 

#Grep
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

#Readable output
alias df="df -h"

#unlocks
#alias unlock="sudo rm /var/lib/pacman/db.lck"
#alias rmlogoutlock="sudo rm /tmp/arcologout.lock"

#Show all unused memory
alias free="free -mt"

#use all cores
alias uac="sh ~/.bin/main/000*"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

#Alias for software managerment
alias pacman="sudo pacman --color=auto"

#ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#update grub
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc="sudo fc-cache -fv"

#switch between bash and zsh
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"

#hardware info
alias hw="hwinfo --short"

#microcode check
alias microcode="grep . /sys/devices/system/cpu/vulnerabilities/*"

#ripgrep
alias rg="rg --sort path"

# ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias psmem='ps auxf | sort -nr -k 4'
alias pscpu='ps auxf | sort -nr -k 3'

# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"

# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

# switch between shells
# I do not recommend switching default SHELL from bash.
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"

# bare git repo alias for dotfiles
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

# NordVPN
alias cities="sudo nordvpn cities"
alias countries="sudo nordvpn countries"
alias connectme="sudo nordvpn connect"
# END
#Use neovim for vim if present
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

vf() { fzf | xargs -r -I $EDITOR % ;}


# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/shasten/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/shasten/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/shasten/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/shasten/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

. "$HOME/.cargo/env"
